
/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/
 
public class cuerpoSnake extends posicionesSnake {
    /** Declaración de variables **/
    private cuerpoSnake prev;
    
    /** Declaración de variables de ubicación del cuerpo del Snake **/
    public cuerpoSnake(int x, int y) {
        super(x, y);
    }
    
    public cuerpoSnake getPrev() {
        return prev;
    }
    
    public void setPrev(cuerpoSnake elem) {
        prev = elem;
    }
}
