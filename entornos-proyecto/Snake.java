
/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/

public class Snake {
    /** Declaración de variables **/
    private cuerpoSnake cabeza;
    private cuerpoSnake cola;
    private final Tablero tablero;
    
    public Snake(int x, int y, Tablero tablero) {
        cabeza = new cuerpoSnake(x, y);
        cola = cabeza;
        this.tablero = tablero;
        tablero.add(cabeza);
    }
    
    private boolean movimiento(cuerpoSnake nuevaCabeza) {
        cabeza.setPrev(nuevaCabeza);
        cabeza = nuevaCabeza;
        
        if(tablero.esManzana(nuevaCabeza.sacarX(), nuevaCabeza.sacarY()))

        if(tablero.esManzana(nuevaCabeza.sacarX(), nuevaCabeza.sacarY()))

            tablero.addManzana();
        else {
            if(tablero.esSnake(nuevaCabeza.sacarX(), nuevaCabeza.sacarY()))
                return false;
            tablero.borrar(cola);
            cola = cola.getPrev();
        }

        tablero.add(nuevaCabeza);
        tablero.add(nuevaCabeza);

        return true;
    }
    
    private int desbordamiento(int valor, int maximo) {
        if(valor < 0)
            valor = maximo + valor;
            valor %= maximo;
            return valor;
        }
    
    /** Declaración del movimiento (Arriba,Abajo,Derecha,Izquierda) **/
    public boolean moverArriba() {
        int posicionY = desbordamiento(cabeza.sacarY() - 1, tablero.sacarAltura());
        int posicionX = cabeza.sacarX();
        return movimiento(new cuerpoSnake(posicionX, posicionY));
    }  
    
    public boolean moverAbajo() {
        int posicionY = desbordamiento(cabeza.sacarY() + 1, tablero.sacarAltura());
        int posicionX = cabeza.sacarY();
        return movimiento(new cuerpoSnake(posicionX, posicionY));
    }
    
    public boolean moverIzquierda() {
        int posicionY = cabeza.sacarY();
        int posicionX = desbordamiento(cabeza.sacarX() - 1, tablero.sacarAncho());
        return movimiento(new cuerpoSnake(posicionX, posicionY));
    }
    
    public boolean moverDerecha() {
        int posicionY = cabeza.sacarY();
        int posicionX = desbordamiento(cabeza.sacarX() + 1, tablero.sacarAncho());
        return movimiento(new cuerpoSnake(posicionX, posicionY));
    }
}