
/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Terminal {
    public static final String tecla_Arriba = new String("\u001B[A"); // "\u001B" ESCAPE + "[A" Flecha Arriba
    public static final String tecla_Abajo = new String("\u001B[B"); // "\u001B" ESCAPE + "[B" Flecha Abajo
    public static final String tecla_Izquierda = new String("\u001B[D"); // "\u001B" ESCAPE + "[D" Flecha Izquierda
    public static final String tecla_Derecha = new String("\u001B[C"); // "\u001B" ESCAPE + "[C" Flecha Derecha
    
    private final InputStreamReader in;
    private final PrintStream out;
    
    public Terminal(InputStream in, PrintStream out) {
        this.in = new InputStreamReader(in);
        this.out = out;
    }
    
    public void limpiar() {
        out.print("\u001B[2J");
    }
    
    public void println(String string) {
        out.println(string);
    }
    
    public void cerrar() {}
    
    public String verPulsacionTecla() {
        char[] buffer = new char[1024];
        String resultado = "";
        
        try {
            if(!in.ready())
                return "";
            int numeroPulsacionesLeidas = in.read(buffer);
            resultado = String.valueOf(buffer, 0, numeroPulsacionesLeidas);
        } catch (IOException e) {
            out.println("Imposible leer la tecla presionada.");
            e.printStackTrace();
        }
        
        return resultado;
    }
    
    public void colocaCursor(int x, int y) {
        out.printf("\u0001B[%d;%dH", y + 1, x + 1);
    }
    
    public void dibujar(String string) {
        out.print(string);
    }
}