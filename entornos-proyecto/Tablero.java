  
/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/
import java.util.Random;

public class Tablero {
    private final int ancho;
    private final int altura;
    private final cuerpoSnake[][] tablero;
    private final Random generador;
    private final vistaTablero vista;
    
    public Tablero(int ancho, int altura, int numeroManzanas, Terminal term) {
        this.ancho = ancho;
        this.altura = altura;
        tablero = new cuerpoSnake[altura][ancho];
        generador = new Random();
        vista = new vistaTablero(tablero, term);
        
        while (numeroManzanas > 0) {
            addManzana();
            numeroManzanas--;
        }
    }
    
    public vistaTablero sacarVista() {
        return vista;
    }
    
    public void addManzana() {
        boolean added = false;
        while (!added) {
            int x = generador.nextInt(ancho);
            int y = generador.nextInt(altura);
        
            if(tablero[y][x] == null) {
                Manzana manzana = new Manzana(x,y);
                tablero[x][y] = manzana;
                added = true;
                vista.add(manzana);
            }
        }  
    }
    
    public int sacarAncho() {
        return ancho;
    }
    
    public int sacarAltura() {
        return altura;
    }
    
    public void add(cuerpoSnake elem) {
        tablero[elem.sacarY()][elem.sacarX()] = elem;
        vista.add(elem);
    }
    
    public boolean esManzana(int x, int y) {
        x %= ancho;
        y %= altura;
        return tablero[y][x] instanceof Manzana;
    }
    
    public boolean esSnake(int x, int y) {
        x %= ancho;
        y %= altura;
        return tablero[y][x] instanceof cuerpoSnake;
    }
    
    public void borrar(cuerpoSnake elem) {
        tablero[elem.sacarY()][elem.sacarX()] = null;
        vista.borrar(elem);
    }
}