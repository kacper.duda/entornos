
/**
* @author Kacper Stanislaw Duda y Mihai George Alexandru 
* @version Versión 1
* 
**/
    
public class Principal {
    
    /*
     * Inicialización y inicio del juego, preparación de la terminal para el usuario final
    */
    public static void principal(String[] args) {
        Terminal term = new Terminal(System.in, System.out);
        Tablero tablero = new Tablero(20, 20, 10, term);
        vistaTablero vista = tablero.sacarVista();
        Snake snake = new Snake(10, 10, tablero);
        
        // INICIO DE LA INTERFAZ
        term.limpiar();
        vista.dibujarBordes();
        vista.mostrarContenido();
        
        ejecutarBucleInicio(term, snake);
        
        // FINAL DEL JUEGO
        term.println("GAME OVER");
        
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {}
        term.cerrar();
    }
    
    @SuppressWarnings("static-access")
    private static void ejecutarBucleInicio(Terminal term, Snake snake) {
        boolean continuarJuego = true;
        String direccion = term.tecla_Arriba;
        
        while (continuarJuego) {
            String tecla = term.verPulsacionTecla();
            
            // Comprueba la tecla pulsada y guarda la dirección del movimiento que va a realizar más adelante
            if (
                term.tecla_Arriba.equals(tecla) ||
                term.tecla_Abajo.equals(tecla)|| 
                term.tecla_Izquierda.equals(tecla)|| 
                term.tecla_Derecha.equals(tecla)) { 
                    direccion = tecla; 
                }
            
            // Comprueba que coincide el movimiento
            if      (term.tecla_Arriba.equals(direccion)) { 
                continuarJuego = snake.moverArriba();    
            }
            else if (term.tecla_Abajo.equals(direccion)) { 
                continuarJuego = snake.moverAbajo();  
            }
            else if (term.tecla_Izquierda.equals(direccion)) { 
                continuarJuego = snake.moverIzquierda();  
            }
            else if (term.tecla_Derecha.equals(direccion)) { 
                continuarJuego = snake.moverDerecha(); 
            }
            
            try { 
                Thread.sleep(250); 
            } catch (InterruptedException e) {}          
        }
    }
}