/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/

public class vistaTablero {
    private final int ancho;
    private final int altura;
    private final Terminal term;
    private final Object[][] tablero;
    
    public vistaTablero(Object[][] tablero,Terminal term) {
        ancho = tablero[0].length;
        altura = tablero.length;
        this.term = term;
        this.tablero = tablero;
    }
    
    public void dibujarBordes(){
        System.out.println(
        "⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌");
        
        System.out.println("         _                            __    __    __    __\n" + 
        "                | |                 /  \\  /  \\  /  \\  /  \\\n" + 
        " ___ _ __   __ _| | _____     ______________/  __\\/  __\\/  __\\/  __\\___________\n" + 
        "/ __| '_ \\ / _` | |/ / _ \\    _____________/  /__/  /__/  /__/  /______________\n" + 
        "\\__ \\ | | | (_| |   <  __/              | / \\   / \\   / \\   / \\  \\____\n" + 
            "|___/_| |_|\\__,_|_|\\_\\___|         |/   \\_/   \\_/   \\_/   \\    âž° \\         \n" + 
        "                                  \\_____/â“â“<");
        System.out.println(
            "⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌⚌");
        
        term.colocaCursor(0,0);
        term.dibujar("/");
        for(int i = 0; i < ancho; i++) { 
            term.dibujar("-"); 
        }
        term.dibujar("\\");
        for (int i = 0; i < altura; i++) {
            term.colocaCursor(0,i + 1);
            term.dibujar("|");
            term.colocaCursor(ancho + 1,i + 1);
            term.dibujar("|");
        }
        term.dibujar("\n");
        term.dibujar("\\");
        for(int i = 0; i < ancho + 1; i++) {
            term.dibujar("-");
        }    
        term.dibujar("/");
    } 
    
    public void add(Manzana manzana) {
    	term.colocaCursor(manzana.sacarX() + 1, manzana.sacarY() + 1);
    	term.dibujar("o");
    }
    
    public void add(cuerpoSnake elem) {
    	term.colocaCursor(elem.sacarX() + 1, elem.sacarY() + 1);
    	term.dibujar("#");
    }
    
    public void borrar(cuerpoSnake elem) {
    	term.colocaCursor(elem.sacarX() + 1, elem.sacarY() + 1);
    	term.dibujar(" ");
    }
    
    public void mostrarContenido() {
    	for (Object[] row : tablero) {
    	    for (Object elem : row) {
    	        if (elem instanceof Manzana) {
        		add((Manzana)elem);
       		}
       		else if (elem instanceof cuerpoSnake) {
       			add((cuerpoSnake)elem);
       		}
            }
        }
    }
}        