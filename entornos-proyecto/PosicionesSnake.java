/**
 * @author Kacper Stanislaw Duda y Mihai George Alexandru 
 * @version Versión 1
 * 
**/

public class posicionesSnake {
    /** Declaración de variables **/
    private final int x;
    private final int y;
    
    /** Declaración de variables de posición **/
    public posicionesSnake(int x, int y) {
    	this.x = x;
    	this.y = y;
    }
    
    public int sacarX() {
        return x;
    }
    public int sacarY() {
        return y;
    }

}
